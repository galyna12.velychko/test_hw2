const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const { JWT_SECRET } = require('../config');
const { User } = require('../models/userModel');

module.exports.registration = async (req, res) => {
    const { username, password } = req.body;
    if (!username) {
        return res.status(400).json({ message: 'Please, enter a username' });
    }
    if (!password) {
        return res.status(400).json({ message: 'Please, enter a password' });
    }

    const user = new User({
        username,
        password: await bcrypt.hash(password, 10),
    });
    await user.save();

    res.json({ message: 'User created successfully' });
};

module.exports.login = async (req, res) => {
    const { username, password } = req.body;

    const user = await User.findOne({ username });

    if (!user) {
        return res
            .status(400)
            .json({ message: `No user with ${username} not found` });
    }
    if (!(await bcrypt.compare(password, user.password))) {
        return res.status(400).json({ message: 'wrong password' });
    }
    const token = jwt.sign(
        { _id: user._id, user: user.username, createdDate: user.createdDate },
        JWT_SECRET
    );
    res.json({ message: 'Success', jwt_token: token });
};
