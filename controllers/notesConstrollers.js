const {Note} = require('../models/noteModels');

module.exports.getAllNotes = async (req, res) => {
  const {offset, limit} = req.body;
  const notes = await Note.find({userId: req.user._id}, {__v: 0})
      .skip(offset)
      .limit(limit);

  res.status(200).json({notes: notes});
};

module.exports.createNote = async (req, res) => {
  const text = req.body.text;
  const userId = req.user._id;
  const note = new Note({
    userId: userId,
    completed: false,
    text: text,
  });
  if (!userId) {
    res.status(400).json({message: 'You should log in.'});
  }
  await note.save();
  res.status(200).json({message: 'Success!'});
};

module.exports.getNotebyId = async (req, res) => {
  const {id} = req.params;
  const note = await Note.findById(id, {_v: 0}).exec();
  res.status(200).json({
    note: {
      _id: note._id,
      userId: note.userId,
      completed: note.completed,
      text: note.text,
      createdDate: note.createdDate,
    },
  });
};

module.exports.putNotebyId = async (req, res) => {
  const {id} = req.params;
  const {text} = req.body;
  const note = await Note.findById(id, {_v: 0}).exec();
  note.text = text;
  await note.save();
  res.status(200).json({message: 'Success'});
};

module.exports.patchNoteById = async (req, res) => {
  const {id} = req.params;
  const note = await Note.findById(id, {_v: 0}).exec();
  note.completed = note.completed === true ? false : true;
  await note.save();
  res.status(200).json({message: 'Success'});
};

module.exports.deleteNotebyId = async (req, res) => {
  const {id} = req.params;
  await Note.findByIdAndRemove(id, req.body, function(err, data) {
    if (!err) {
      res.status(200).json({message: 'Success'});
    }
  });
};
