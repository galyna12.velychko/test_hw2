const mongoose = require('mongoose');
const express = require('express');
const morgan = require('morgan');
const app = express();

const authRouter = require('./routers/authRouter');
const userProfileRoute = require('./routers/userProfileRoute');
const notesRoute = require('./routers/notesRoute');

app.use(express.json());
app.use(express.static('build'));
app.use(morgan('tiny'));

app.use('/api/auth', authRouter);
app.use('/api/users', userProfileRoute);
app.use('/api', notesRoute);

/** Class representing a statusCode. */
class UnauthorizedError extends Error {
  /**
     * @param {string} message - The message value.
     */
  constructor(message = 'Unauthorized user') {
    super(message);
    statusCode = 400;
  }
}

app.use((err, req, res, next) => {
  if (err instanceof UnauthorizedError) {
    res.status(err.statusCode).json({message: err.message});
  }
  res.status(500).json({message: err.message});
});

const start = async () => {
  await mongoose.connect(
      'mongodb+srv://Halyna-12345user:akilag24@cluster0.6rwbo.mongodb.net/hw_2_new_db',
      {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useFindAndModify: false,
        useCreateIndex: true,
      },
  );
  const PORT = process.env.PORT || 8080;
  app.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}`);
  });
};

start();
