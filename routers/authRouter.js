const express = require('express');
const router = new express.Router();

const {asyncWrapper} = require('./helpers');
const {validateRegistration} = require('./middleware/validationMiddleware');
const {login, registration} = require('../controllers/authControllers');

router.post(
    '/register',
    asyncWrapper(validateRegistration),
    asyncWrapper(registration),
);
router.post('/login', asyncWrapper(login));

module.exports = router;
