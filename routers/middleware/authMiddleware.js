const jwt = require('jsonwebtoken');
const {JWT_SECRET} = require('../../config');

module.exports.authMiddleware = (req, res, next) => {
  const header = req.headers['authorization'];

  if (!header) {
    return res
        .status(400)
        .json({message: `No authorization http header found`});
  }

  const [tokenType, token] = header.split(' ');

  console.log(tokenType);

  if (!token) {
    return res.status(400).json({message: `No JWT token found`});
  }
  try {
    req.user = jwt.verify(token, JWT_SECRET);
    next();
  } catch (err) {
    return res.status(400).json({message: 'Bad token'});
  }
};

module.exports.checkModel = (Model) => async (req, res, next) => {
  const {id} = req.params;

  try {
    if (!(await Model.exists({_id: id}))) {
      return res.status(400).json({message: 'No note with this id'});
    }
    next();
  } catch (e) {
    return res.status(400).json({message: 'No note with this id'});
  }
};
