const express = require('express');
const router = new express.Router();
const {authMiddleware, checkModel} = require('./middleware/authMiddleware');
const {Note} = require('../models/noteModels');
const {asyncWrapper} = require('./helpers');
const {
  getAllNotes,
  createNote,
  getNotebyId,
  deleteNotebyId,
  putNotebyId,
  patchNoteById,
} = require('../controllers/notesConstrollers');

router.get('/notes', authMiddleware, asyncWrapper(getAllNotes));
router.post('/notes', authMiddleware, asyncWrapper(createNote));
router.get(
    '/notes/:id',
    authMiddleware,
    checkModel(Note),
    asyncWrapper(getNotebyId),
);
router.put(
    '/notes/:id',
    authMiddleware,
    checkModel(Note),
    asyncWrapper(putNotebyId),
);
router.patch(
    '/notes/:id',
    authMiddleware,
    checkModel(Note),
    asyncWrapper(patchNoteById),
);
router.delete(
    '/notes/:id',
    authMiddleware,
    checkModel(Note),
    asyncWrapper(deleteNotebyId),
);

module.exports = router;
