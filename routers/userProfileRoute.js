const express = require('express');
const router = new express.Router();
const {User} = require('../models/userModel');
const {authMiddleware} = require('./middleware/authMiddleware');
const bcrypt = require('bcrypt');

//  api/users/me
router.get('/me', authMiddleware, async (req, res) => {
  const userMe = await User.findById(req.user._id).exec();
  return res.status(200).json({
    user: {
      _id: userMe._id,
      username: userMe.username,
      createdDate: userMe.createdDate,
    },
  });
});

router.delete('/me', authMiddleware, async (req, res) => {
  await User.findByIdAndRemove(req.user._id, function(err, data) {
    if (!err) {
      return res.status(200).json({message: 'Success'});
    }
  });
});

router.patch('/me', authMiddleware, async (req, res) => {
  const {oldPassword, newPassword} = req.body;
  const user = await User.findById(req.user._id, 'password').exec();

  if (!(await bcrypt.compare(oldPassword, user.password))) {
    return res.status(400).json({message: 'wrong password'});
  }
  user.password = await bcrypt.hash(newPassword, 10);
  await user.save();

  res.status(200).json({message: 'Success'});
});

module.exports = router;
